/*
 * FrmPrincipal.java
 *
 * Created on 11-abr-2010, 8:30:44
 */
package co.belisariodelamata.culebra.interfaz;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import co.belisariodelamata.culebra.juego.Juego;
import co.belisariodelamata.culebra.juego.Juego.Modo;

/**
 * @author BS
 */
public class FrmPrincipal extends javax.swing.JFrame {

    javax.swing.JCheckBoxMenuItem modoAnterior = null;//Nivel anterior
    javax.swing.JCheckBoxMenuItem menuNivelAnterior = null;//Nivel anterior
    List<JCheckBoxMenuItem> menuNiveles = new ArrayList();

    Juego juego = new Juego();

    public FrmPrincipal() {
        initComponents();
        initComponentPanel();
        cargarMenuLaberintos();
        cargarMenuNiveles();
        cargarMenuModos();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (juego != null) {
            juego.repintar();
        }
    }

    private void initComponentPanel() {
        panelJuego = new JPanel();
        panelJuego.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelJuego.setBackground(new java.awt.Color(255, 255, 255));

        panelJuego.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            @SuppressWarnings("empty-statement")
            public void mousePressed(java.awt.event.MouseEvent evt) {
                if (menuModoDiseno.isSelected()) {
                    if (evt.getButton() == 1) {
                        juego.validarYDibujarBloqueCoordenadaPixel(evt.getX(), evt.getY());
                    } else {
                        juego.validarYBorrarBloqueCoordenadaPixel(evt.getX(), evt.getY());
                    }
                }
            }
        });

        panelJuego.setLayout(null);

        getContentPane().add(panelJuego);

        int panelWidth = this.getWidth() - 100;
        int panelHeight = panelWidth * 14 / 22;

        panelJuego.setBounds(this.getWidth() / 2 - (panelWidth / 2), 50, panelWidth, panelHeight);
        panelJuego.setDoubleBuffered(true);
        juego.setFrame(this);
        juego.setLblBono(lblBono);
        juego.setLblProgresoVitamina(lblProgresoVitamina);
        juego.setLblPuntaje(lblPuntaje);
        juego.setPanelJuego(panelJuego);

    }

    private void cargarMenuModos() {
        JCheckBoxMenuItem modos;
        for (int x = 1; x <= 2; x++) {
            modos = new JCheckBoxMenuItem();
            modos.setName(x + "");
            if (x == 1) {
                modos.setText("Clásico");
            } else if (x == 2) {
                modos.setText("Campaña");
            }

            if (x == juego.getModoDeJuego().ordinal() + 1) {
                modos.setState(true);
                modoAnterior = modos;
            }
            menuModo.add(modos);
            modos.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    menuModosClick(e);
                }
            });
        }
    }

    private void menuModosClick(ActionEvent e) {
        JCheckBoxMenuItem menu = (javax.swing.JCheckBoxMenuItem) e.getSource();
        int indexModo = Integer.parseInt(menu.getName());
        if (indexModo == juego.getModoDeJuego().ordinal() + 1) {
            modoAnterior.setState(true);
            return;
        }
        if (modoAnterior != null) {
            modoAnterior.setState(false);
        }
        if (indexModo == 1) {
            juego.setModoDeJuego(Juego.Modo.CLASICO);
        } else {
            juego.setModoDeJuego(Juego.Modo.CAMPANA);
        }
        modoAnterior = menu;
        menu.setState(true);
        if (indexModo == 1) {
            menuLaberintos.setVisible(true);
            menuNivelClick(new ActionEvent(menuNiveles.get(juego.getNivelClasico() - 1), juego.getNivelClasico(), null));
        } else if (indexModo == 2) {
            menuLaberintos.setVisible(false);
            menuNivelClick(new ActionEvent(menuNiveles.get(juego.getNivelCampana() - 1), juego.getNivelCampana(), null));
        }
    }

    private void cargarMenuNiveles() {
        JCheckBoxMenuItem niveles;
        for (int x = 1; x <= 14; x++) {
            niveles = new JCheckBoxMenuItem(x + "");
            niveles.setName(x + "");
            menuNivel.add(niveles);
            menuNiveles.add(niveles);
            if (x == juego.getNivelActual()) {
                menuNivelAnterior = niveles;
                niveles.setState(true);
            }
            niveles.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    menuNivelClick(e);
                }
            });
        }
    }

    private void menuNivelClick(ActionEvent e) {
        JCheckBoxMenuItem menu = (JCheckBoxMenuItem) e.getSource();
        int nivelDelMenu = Integer.parseInt(menu.getName());
        if (nivelDelMenu == juego.getNivelActual()) {
            menuNivelAnterior.setState(true);
        } else {
            if (menuNivelAnterior != null) {
                menuNivelAnterior.setState(false);
            }
            menuNivelAnterior = menu;
            if (juego.getModoDeJuego().equals(Modo.CLASICO)) {
                juego.setNivelClasico((byte) nivelDelMenu);
            } else if (juego.getModoDeJuego().equals(Modo.CAMPANA)) {
                juego.setNivelCampana((byte) nivelDelMenu);
            }
            juego.setNivelActual((byte) nivelDelMenu);
            menu.setState(true);
        }
        repaint();
    }

    private void cargarMenuLaberintos() {
        javax.swing.JCheckBoxMenuItem laberintos;
        for (int x = 0; x <= 11; x++) {
            laberintos = new javax.swing.JCheckBoxMenuItem();
            if (x == 0) {
                laberintos.setText("Sin Laberinto");
            } else {
                laberintos.setText(x + "");
            }
            laberintos.setName(x + "");
            if (x == juego.getLaberintoActualCampana()) {
                laberintos.setState(true);
                labAnt = laberintos;
            }
            menuLaberintos.add(laberintos);
            laberintos.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    laberintosClick(e);
                }
            });
        }
    }
    private javax.swing.JCheckBoxMenuItem labAnt = null;//laberinto anterior

    private void laberintosClick(ActionEvent e) {
        juego.terminar();
        JCheckBoxMenuItem menuNivelLaberinto = (JCheckBoxMenuItem) e.getSource();
        int indexLaberinto = Integer.parseInt(menuNivelLaberinto.getName());
        if (indexLaberinto == juego.getLaberintoActualClasico()) {
            labAnt.setState(true);
            return;
        }
        if (labAnt != null) {
            labAnt.setState(false);
        }
        labAnt = menuNivelLaberinto;
        juego.setPanelJuego(panelJuego);
        juego.dibujarLaberintoMuestra(indexLaberinto);
        menuNivelLaberinto.setState(true);
        juego.setLaberintoActualClasico(Byte.parseByte(menuNivelLaberinto.getName()));
        repaint();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblProgresoVitamina = new javax.swing.JLabel();
        lblPuntaje = new javax.swing.JLabel();
        lblBono = new javax.swing.JLabel();
        menuPrincipal = new javax.swing.JMenuBar();
        bsMenu = new javax.swing.JMenu();
        menuNuevoJuego = new javax.swing.JMenuItem();
        menuModo = new javax.swing.JMenu();
        menuNivel = new javax.swing.JMenu();
        menuLaberintos = new javax.swing.JMenu();
        menuDesarrollador = new javax.swing.JMenu();
        menuModoDiseno = new javax.swing.JCheckBoxMenuItem();
        menuBusquedaInteligente = new javax.swing.JCheckBoxMenuItem();
        menuMostrarEnConsola = new javax.swing.JCheckBoxMenuItem();
        menuCrecerAlComer = new javax.swing.JCheckBoxMenuItem();
        menuContinuarJuego = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Belsoft");
        setBackground(new java.awt.Color(0, 0, 0));
        setForeground(new java.awt.Color(255, 255, 255));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });
        getContentPane().setLayout(null);

        lblProgresoVitamina.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        lblProgresoVitamina.setForeground(new java.awt.Color(0, 0, 255));
        lblProgresoVitamina.setText("progreso vitamina");
        lblProgresoVitamina.setVisible(false);
        getContentPane().add(lblProgresoVitamina);
        lblProgresoVitamina.setBounds(630, 10, 300, 30);

        lblPuntaje.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        lblPuntaje.setForeground(new java.awt.Color(255, 0, 51));
        lblPuntaje.setText("Hola, soy Belisario");
        getContentPane().add(lblPuntaje);
        lblPuntaje.setBounds(30, 10, 300, 29);

        lblBono.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        lblBono.setForeground(new java.awt.Color(0, 0, 204));
        lblBono.setText("Bono");
        lblBono.setVisible(false);
        getContentPane().add(lblBono);
        lblBono.setBounds(330, 10, 300, 30);

        bsMenu.setText("Juego");
        bsMenu.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                bsMenuMenuSelected(evt);
            }
        });

        menuNuevoJuego.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        menuNuevoJuego.setText("Juego Nuevo");
        menuNuevoJuego.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuNuevoJuegoActionPerformed(evt);
            }
        });
        bsMenu.add(menuNuevoJuego);

        menuModo.setText("Modo");
        bsMenu.add(menuModo);

        menuNivel.setText("Nivel");
        bsMenu.add(menuNivel);

        menuLaberintos.setText("Laberintos");
        bsMenu.add(menuLaberintos);

        menuPrincipal.add(bsMenu);

        menuDesarrollador.setText("Desarrollador");
        menuDesarrollador.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                menuDesarrolladorMenuSelected(evt);
            }
        });

        menuModoDiseno.setSelected(true);
        menuModoDiseno.setText("Modo diseño");
        menuDesarrollador.add(menuModoDiseno);

        menuBusquedaInteligente.setText("** Búsqueda automática **");
        menuBusquedaInteligente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBusquedaInteligenteActionPerformed(evt);
            }
        });
        menuDesarrollador.add(menuBusquedaInteligente);

        menuMostrarEnConsola.setText("Mostrar en Consola");
        menuMostrarEnConsola.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuMostrarEnConsolaActionPerformed(evt);
            }
        });
        menuDesarrollador.add(menuMostrarEnConsola);

        menuCrecerAlComer.setSelected(true);
        menuCrecerAlComer.setText("Crecer al comer");
        menuCrecerAlComer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCrecerAlComerActionPerformed(evt);
            }
        });
        menuDesarrollador.add(menuCrecerAlComer);

        menuContinuarJuego.setText("Continuar");
        menuContinuarJuego.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuContinuarJuegoActionPerformed(evt);
            }
        });
        menuDesarrollador.add(menuContinuarJuego);

        menuPrincipal.add(menuDesarrollador);

        setJMenuBar(menuPrincipal);

        setSize(new java.awt.Dimension(1008, 702));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void menuNuevoJuegoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuNuevoJuegoActionPerformed
        juego.setBusquedaAutomatica(menuBusquedaInteligente.isSelected());
        juego.setCrecerAlComer(menuCrecerAlComer.isSelected());
        juego.setMostrarEnConsola(menuMostrarEnConsola.isSelected());
        juego.nuevoJuego();

    }//GEN-LAST:event_menuNuevoJuegoActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        teclaPresionada(evt);
    }//GEN-LAST:event_formKeyPressed

    private void teclaPresionada(java.awt.event.KeyEvent evt) {
        this.juego.keyPressed(evt.getKeyCode());
    }

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        JOptionPane.showMessageDialog(rootPane, "Belsoft Inmaginations", "Bs/Record", JOptionPane.INFORMATION_MESSAGE);
        juego.presentacion();
    }//GEN-LAST:event_formWindowOpened

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        if (juego != null) {
            juego.repintar();
        }
    }//GEN-LAST:event_formWindowActivated

    private void bsMenuMenuSelected(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_bsMenuMenuSelected
        juego.pausar();
    }//GEN-LAST:event_bsMenuMenuSelected

    private void menuContinuarJuegoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuContinuarJuegoActionPerformed
        juego.retomarJuego();
    }//GEN-LAST:event_menuContinuarJuegoActionPerformed

    private void menuDesarrolladorMenuSelected(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_menuDesarrolladorMenuSelected
        juego.pausar();
    }//GEN-LAST:event_menuDesarrolladorMenuSelected

    private void menuBusquedaInteligenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBusquedaInteligenteActionPerformed
        juego.setBusquedaAutomatica(menuBusquedaInteligente.isSelected());
        juego.retomarJuego();
    }//GEN-LAST:event_menuBusquedaInteligenteActionPerformed

    private void menuCrecerAlComerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCrecerAlComerActionPerformed
        juego.setCrecerAlComer(menuCrecerAlComer.isSelected());
        juego.retomarJuego();
    }//GEN-LAST:event_menuCrecerAlComerActionPerformed

    private void menuMostrarEnConsolaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuMostrarEnConsolaActionPerformed
        juego.setMostrarEnConsola(menuMostrarEnConsola.isSelected());
        juego.retomarJuego();
    }//GEN-LAST:event_menuMostrarEnConsolaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmPrincipal().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu bsMenu;
    private javax.swing.JLabel lblBono;
    private javax.swing.JLabel lblProgresoVitamina;
    private javax.swing.JLabel lblPuntaje;
    private javax.swing.JCheckBoxMenuItem menuBusquedaInteligente;
    private javax.swing.JMenuItem menuContinuarJuego;
    private javax.swing.JCheckBoxMenuItem menuCrecerAlComer;
    private javax.swing.JMenu menuDesarrollador;
    private javax.swing.JMenu menuLaberintos;
    private javax.swing.JMenu menuModo;
    private javax.swing.JCheckBoxMenuItem menuModoDiseno;
    private javax.swing.JCheckBoxMenuItem menuMostrarEnConsola;
    private javax.swing.JMenu menuNivel;
    private javax.swing.JMenuItem menuNuevoJuego;
    private javax.swing.JMenuBar menuPrincipal;
    // End of variables declaration//GEN-END:variables
    private JPanel panelJuego;
}
