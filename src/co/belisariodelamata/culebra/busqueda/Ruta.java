package co.belisariodelamata.culebra.busqueda;

import co.belisariodelamata.culebra.juego.Direccion;
import co.belisariodelamata.culebra.juego.Posicion;
import co.belisariodelamata.culebra.juego.PosicionDireccion;
import java.util.LinkedList;
import java.util.List;
import co.belisariodelamata.culebra.juego.IConfiguracionLaberinto;
import java.util.Arrays;

/**
 *
 * @author BELSOFT
 */
public class Ruta {

    List<Posicion> posicionesLibres = new LinkedList<>();

    IConfiguracionLaberinto referenciaLaberinto;

    public Ruta(IConfiguracionLaberinto referenciaLaberinto) {
        this.referenciaLaberinto = referenciaLaberinto;
    }

    public void addPosicionesLibres(Posicion... posiciones) {
        posicionesLibres.addAll(Arrays.asList(posiciones));
    }

    private void siguientePosicion(PosicionDireccion posicionReferencia) {
        referenciaLaberinto.siguientePosicion(posicionReferencia);
    }

    public Direccion seleccionarDireccion(Posicion posicionInicial, Posicion posicionFinal) {
        return seleccionarDireccion(posicionInicial, posicionFinal, null);
    }

    /**
     *
     * @param posicionInicial
     * @param posicionFinal
     * @param numeroPasosMaximo: Número de pasos para tener en cuenta la ruta.
     * Valor null para no considerarlo
     * @return
     */
    public Direccion seleccionarDireccion(Posicion posicionInicial, Posicion posicionFinal, Integer numeroPasosMaximo) {
        Direccion direccionSeleccionada = null;
        List<BusquedaDireccion> rutasEncontradas = buscarRutas(posicionInicial, posicionFinal);
        BusquedaDireccion busquedaDireccionSeleccionada = null;

        //Solo se encontró una dirección
        if (rutasEncontradas.size() == 1) {
            busquedaDireccionSeleccionada = rutasEncontradas.get(0);
            //Hay al menos dos rutas encontradas
        } else if (rutasEncontradas.size() >= 2) {
            //Las dos rutas tienes la misma cantidad de pasos
            //Se seleccionada alguna de las dos de forma aleatoria
            if (rutasEncontradas.get(0).getNumeroPasos() == rutasEncontradas.get(1).getNumeroPasos()) {
                if (Math.random() > 0.5) {
                    busquedaDireccionSeleccionada = rutasEncontradas.get(0);
                } else {
                    busquedaDireccionSeleccionada = rutasEncontradas.get(1);
                }
            } else {
                //Si las dos primeras rutas no tienen la misma cantidad de pasos
                //Se selecciona la primera porque es la que tiene menos pasos
                busquedaDireccionSeleccionada = rutasEncontradas.get(0);
            }
        }
        //Se tiene en cuenta la ruta si el número de pasos es menor al que se necesita
        if (busquedaDireccionSeleccionada != null
                && (numeroPasosMaximo == null || busquedaDireccionSeleccionada.getNumeroPasos() <= numeroPasosMaximo)) {
            direccionSeleccionada = busquedaDireccionSeleccionada.getDireccionInicial();
        }
        return direccionSeleccionada;
    }

    public List<BusquedaDireccion> buscarRutas(Posicion posicionInicial, Posicion posicionFinal) {
        return buscarRutas(posicionInicial, posicionFinal, null);
    }

    public List<BusquedaDireccion> buscarRutas(Posicion posicionInicial, Posicion posicionFinal, Direccion direccionIgnorada) {
        List<BusquedaDireccion> rutasEncontradas = new LinkedList<>();
        //Revisa las 4 Direcciones Principales e Ignora la Pasada como Parametro
        for (Direccion direccionPosible : Direccion.values()) {
            if (direccionIgnorada != null && direccionPosible.equals(direccionIgnorada)) {
                continue;
            }
            BusquedaDireccion busquedaDireccion = buscarRuta(posicionInicial, posicionFinal, direccionPosible);
            if (busquedaDireccion != null) {
                rutasEncontradas.add(busquedaDireccion);
            }
        }
        //Se ordena por número de pasos
        rutasEncontradas.sort((r1, r2) -> r1.getNumeroPasos() - r2.getNumeroPasos());
        return rutasEncontradas;
    }

    public BusquedaDireccion buscarRuta(Posicion posicionInicial, Posicion posicionFinal, Direccion direccionPrincipal) {
        boolean[][] casillasOcupadasTmp = new boolean[referenciaLaberinto.getPosicionesEnX()][referenciaLaberinto.getPosicionesEnY()];
        BusquedaDireccion busquedaDireccionEncontrada = null;

        //Se Marca que la casilla de Origen está ocupada
        marcarCasillaRecorrida(casillasOcupadasTmp, posicionInicial);

        //Se crea un lista de las direcciones que serán evaluadas
        List<BusquedaDireccion> listaDireccionesParaEvaluar = new LinkedList<>();
        //Se añade la posicion inicial como la primera casilla a evaluar
        listaDireccionesParaEvaluar.add(new BusquedaDireccion(posicionInicial));

        //Si se especificó una dirección principal entonces se marca que inicialmente esa es la que se tendrá en cuenta para la validación
        boolean evaluarSoloDireccionPrincipal = direccionPrincipal != null;

        //Se recorre mientras haya posiciones para evaluar
        ROMPER_BUSQUEDA:
        while (!listaDireccionesParaEvaluar.isEmpty()) {
            //Se crea una lista con nuevas posiciones para ser evaluadas
            List<BusquedaDireccion> listaDireccionesPorEvaluar = new LinkedList<>();
            //Se recorren todas las posiciones para evaluar
            for (BusquedaDireccion posicionEnEvaluacion : listaDireccionesParaEvaluar) {
                //Se recorren las direcciones posibles desde la posicion para evaluar
                for (Direccion direccionPosible : Direccion.values()) {
                    //Si solo se quiere validar la direccion principal y la direccion no coincide con ella
                    //entonces se termina la iteración actual
                    if (evaluarSoloDireccionPrincipal && !direccionPosible.equals(direccionPrincipal)) {
                        continue;
                    }

                    posicionEnEvaluacion = posicionEnEvaluacion.clone();
                    posicionEnEvaluacion.setDireccion(direccionPosible);

                    //Se crea una nueva posición que contemple la dirección
                    BusquedaDireccion nuevaPosicionDireccion = new BusquedaDireccion(posicionEnEvaluacion, direccionPosible);
                    siguientePosicion(nuevaPosicionDireccion);

                    nuevaPosicionDireccion.setBusquedaDireccionPadre(posicionEnEvaluacion);
                    nuevaPosicionDireccion.setNumeroPasos(posicionEnEvaluacion.getNumeroPasos() + 1);
                    //Se realiza un movimiento hacia la dirección asignada
                    //Si la posición está libre es porque se está construyendo un posible camino
                    if (posicionFinal.equals(nuevaPosicionDireccion)) {
                        busquedaDireccionEncontrada = nuevaPosicionDireccion;
                        break ROMPER_BUSQUEDA;
                    } else if (estaPosicionLibre(nuevaPosicionDireccion, casillasOcupadasTmp)) {
                        //Se ocupa la posición
                        marcarCasillaRecorrida(casillasOcupadasTmp, nuevaPosicionDireccion);
                        //Se añade la posición a las que necesitas ser evualadas en proximo ciclo
                        listaDireccionesPorEvaluar.add(nuevaPosicionDireccion);
                    }
                }
                //Luego que se realiza el Primer Ciclo de Direcciones no es necesario evaluar sólo la dirección principal
                evaluarSoloDireccionPrincipal = false;
            }
            //Se reemplaza la referencia de las posiciones por evaluar
            listaDireccionesParaEvaluar = listaDireccionesPorEvaluar;
        }
        return busquedaDireccionEncontrada;
    }

    public List<BusquedaDireccion> calcularEspaciosEnDireccion(Posicion posicionInicial) {
        List<BusquedaDireccion> rutasEncontradas = new LinkedList<>();
        //Revisa las 4 Direcciones Principales
        for (Direccion direccionPosible : Direccion.values()) {
            int numEspaciosEnDireccion = calcularEspaciosEnDireccion(posicionInicial, direccionPosible);
            BusquedaDireccion busquedaEspacios = new BusquedaDireccion(posicionInicial, direccionPosible);
            busquedaEspacios.setNumeroPasos(numEspaciosEnDireccion);
            rutasEncontradas.add(busquedaEspacios);
        }
        //Se ordena por número de pasos
        rutasEncontradas.sort((r1, r2) -> (r1.getNumeroPasos() - r2.getNumeroPasos()) * -1);
        return rutasEncontradas;
    }

    public int calcularEspaciosEnDireccion(Posicion posicionInicial, Direccion direccionPrincipal) {
        int numeroEspacios = 0;

        boolean[][] casillasRecorridas = new boolean[referenciaLaberinto.getPosicionesEnX()][referenciaLaberinto.getPosicionesEnY()];

        //Se Marca que la casilla de Origen está ocupada
        marcarCasillaRecorrida(casillasRecorridas, posicionInicial);

        //Se crea un lista de las direcciones que serán evaluadas
        List<BusquedaDireccion> listaDireccionesParaEvaluar = new LinkedList<>();
        //Se añade la posicion inicial como la primera casilla a evaluar
        listaDireccionesParaEvaluar.add(new BusquedaDireccion(posicionInicial));

        //Si se especificó una dirección principal entonces se marca que inicialmente esa es la que se tendrá en cuenta para la validación
        boolean evaluarSoloDireccionPrincipal = true;

        //Se recorre mientras haya posiciones para evaluar
        ROMPER_BUSQUEDA:
        while (!listaDireccionesParaEvaluar.isEmpty()) {
            //Se crea una lista con nuevas posiciones para ser evaluadas
            List<BusquedaDireccion> listaDireccionesPorEvaluar = new LinkedList<>();
            //Se recorren todas las posiciones para evaluar
            for (BusquedaDireccion posicionEnEvaluacion : listaDireccionesParaEvaluar) {
                //Se recorren las direcciones posibles desde la posicion para evaluar
                for (Direccion direccionPosible : Direccion.values()) {
                    //Si solo se quiere validar la direccion principal y la direccion no coincide con ella
                    //entonces se termina la iteración actual
                    if (evaluarSoloDireccionPrincipal && !direccionPosible.equals(direccionPrincipal)) {
                        continue;
                    }

                    posicionEnEvaluacion = posicionEnEvaluacion.clone();
                    posicionEnEvaluacion.setDireccion(direccionPosible);

                    //Se crea una nueva posición que contemple la dirección
                    BusquedaDireccion nuevaPosicionDireccion = new BusquedaDireccion(posicionEnEvaluacion, direccionPosible);
                    //Se realiza un movimiento hacia la dirección asignada
                    siguientePosicion(nuevaPosicionDireccion);

                    nuevaPosicionDireccion.setBusquedaDireccionPadre(posicionEnEvaluacion);
                    nuevaPosicionDireccion.setNumeroPasos(posicionEnEvaluacion.getNumeroPasos() + 1);
                    //Si la posición está libre se aumenta el contador de casillas disponibles
                    if (estaPosicionLibre(nuevaPosicionDireccion, casillasRecorridas)) {
                        numeroEspacios++;
                        //Se ocupa la posición
                        marcarCasillaRecorrida(casillasRecorridas, nuevaPosicionDireccion);
                        //Se añade la posición a las que necesitas ser evualadas en proximo ciclo
                        listaDireccionesPorEvaluar.add(nuevaPosicionDireccion);
                    }
                }
                //Luego que se realiza el Primer Ciclo de Direcciones no es necesario evaluar sólo la dirección principal
                evaluarSoloDireccionPrincipal = false;
            }
            listaDireccionesParaEvaluar = listaDireccionesPorEvaluar;
        }
        return numeroEspacios;
    }

    /**
     * Se considera que la posición es Libre si se encuentra en el Listado. O si
     * la casilla del laberinto no está ocupada y tampoco las temporales de la
     * busqueda
     *
     * @param posicion
     * @param casillasRecorridas
     * @return
     */
    private boolean estaPosicionLibre(Posicion posicion, boolean[][] casillasRecorridas) {
        //Si la Posición está entre las declaradas libres y Aún no han sido recorridas
        return (posicionesLibres.contains(posicion)
                && !casillasRecorridas[posicion.getX()][posicion.getY()])
                //Si la posición no se ha marcado como recorrida o no está ocupada
                || !(casillasRecorridas[posicion.getX()][posicion.getY()]
                || referenciaLaberinto.isCasillaOcupada(posicion.getX(), posicion.getY()));
    }

    private static void marcarCasillaRecorrida(boolean[][] casillasRecorridas, Posicion posicion) {
        casillasRecorridas[posicion.getX()][posicion.getY()] = true;
    }
}
