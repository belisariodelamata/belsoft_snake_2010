package co.belisariodelamata.culebra.busqueda;

import co.belisariodelamata.culebra.juego.Direccion;
import co.belisariodelamata.culebra.juego.Posicion;
import co.belisariodelamata.culebra.juego.PosicionDireccion;

/**
 *
 * @author BELSOFT
 */
public class BusquedaDireccion extends PosicionDireccion {

    private BusquedaDireccion busquedaDireccionPadre;

    private int numeroPasos;

    public BusquedaDireccion(Posicion posicion) {
        super(posicion);
    }

    public BusquedaDireccion(Posicion posicion, Direccion direccion) {
        super(posicion, direccion);
    }

    public BusquedaDireccion getBusquedaDireccionPadre() {
        return busquedaDireccionPadre;
    }

    public void setBusquedaDireccionPadre(BusquedaDireccion busquedaDireccionPadre) {
        this.busquedaDireccionPadre = busquedaDireccionPadre;
    }

    public int getNumeroPasos() {
        return numeroPasos;
    }

    public void setNumeroPasos(int numeroPasos) {
        this.numeroPasos = numeroPasos;
    }

    public Direccion getDireccionInicial() {
        BusquedaDireccion tmp = this;
        while (tmp.getBusquedaDireccionPadre() != null) {
            tmp = tmp.getBusquedaDireccionPadre();
        }
        return tmp.getDireccion();
    }

    @Override
    public BusquedaDireccion clone() {
        BusquedaDireccion busquedaDireccion = new BusquedaDireccion(this, this.getDireccion());
        busquedaDireccion.busquedaDireccionPadre = this.busquedaDireccionPadre;
        busquedaDireccion.numeroPasos = this.numeroPasos;
        return busquedaDireccion;
    }
}
