package co.belisariodelamata.culebra.juego;

import java.util.ArrayList;

/**
 * @author BS.Corp
 */
public class Culebra {

    ArrayList<PosicionDireccion> cuerpo = new ArrayList();

    private PosicionDireccion ultimaColaEliminada;

    public void setCabeza(PosicionDireccion nuevaCabeza) {
        cuerpo.clear();
        cuerpo.add(nuevaCabeza);
    }

    /**
     * Añade una Cabeza con las mismas características de la existente
     *
     * @return la nueva instancia de la Cabeza
     */
    public PosicionDireccion agregarCabeza() {
        PosicionDireccion nuevaCabeza = getCabeza().clone();
        agregarCabeza(nuevaCabeza);
        return nuevaCabeza;
    }

    public void agregarCabeza(PosicionDireccion nuevaCabeza) {
        this.cuerpo.add(0, nuevaCabeza);
    }

    public void eliminarCola() {
        if (!cuerpo.isEmpty()) {
            ultimaColaEliminada = this.getCola();
            cuerpo.remove(cuerpo.size() - 1);
        }
    }

    public PosicionDireccion getCabeza() {
        return getCuerpoEnPosicion(0);
    }

    public PosicionDireccion getCuerpoDespuesCabeza() {
        return getCuerpoEnPosicion(1);
    }

    public PosicionDireccion getCuerpoEnPosicion(int indexPosicionCuerpo) {
        return cuerpo.get(indexPosicionCuerpo);
    }

    public PosicionDireccion getCola() {
        return cuerpo.get(cuerpo.size() - 1);
    }

    public PosicionDireccion ultimaColaEliminada() {
        return ultimaColaEliminada;
    }

    public int getTamano() {
        return cuerpo.size();
    }

    public boolean tienePosicion(Posicion posicion) {
        boolean sw = false;
        for (Posicion posicionCulebra : cuerpo) {
            if (posicionCulebra.equals(posicion)) {
                sw = true;
                break;
            }
        }
        return sw;
    }

    public boolean tienePosicion(int x, int y) {
        return tienePosicion(new Posicion(x, y));
    }

    public boolean hayCuerpo() {
        return !this.cuerpo.isEmpty();
    }

}
