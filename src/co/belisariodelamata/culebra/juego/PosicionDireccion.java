package co.belisariodelamata.culebra.juego;

/**
 * Un Objeto Posición que tiene una Direccion
 *
 * @author BELSOFT
 */
public class PosicionDireccion extends Posicion {

    private Direccion direccion;

    public PosicionDireccion() {
    }

    public PosicionDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public PosicionDireccion(Posicion posicion) {
        super(posicion.getX(), posicion.getY());
    }

    public PosicionDireccion(int x, int y) {
        super(x, y);
    }

    public PosicionDireccion(Posicion posicion, Direccion direccion) {
        this(posicion.getX(), posicion.getY(), direccion);
    }

    public PosicionDireccion(int x, int y, Direccion direccion) {
        super(x, y);
        this.direccion = direccion;
    }

    public PosicionDireccion(int x, int y, int itDireccion) {
        super(x, y);
        this.direccion = Direccion.valueOfNumber(itDireccion);
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(int itDireccion) {
        this.direccion = Direccion.valueOfNumber(itDireccion);
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public boolean igualPosicion(Posicion nc) {
        return this.equals(nc);
    }

    /**
     * Cambia la dirección del elemento a la pasada como parámetro. El método es
     * ignorado si se intenta cambiar a una dirección que va en contra de la
     * dirección actual. Por ejemplo, si el elemento va hacia la derecha, no se
     * debe permitir dirigirse a la izquierda.
     *
     * @param itDireccion
     */
    public void cambiarDireccionAbsoluta(int itDireccion) {
        //cambia la direccion absoluta, si recibe 1 dobla hacia la derecha, esté apuntando a donde sea la direccion
        if (getDireccionRelativa(2) != itDireccion) {//si la direccion a donde quiere apuntar no es la contraria de la que tiene
            setDireccion(itDireccion);
        }
    }

    /**
     * @param itRotacion: numero de veces que rota la dirección en sentido
     * inverso a las manecilas del reloj. Ejemplo: Si la posición es hacia abajo
     * ↓, entonces una iteración de 1 la mueve hacia la derecha →, una iteración
     * de 2 la mueve hacia arriba ↑
     * @return La nueva Direccion
     */
    public int getDireccionRelativa(int itRotacion) {
        return Direccion.getDireccionInversaRelativa(this.getDireccion().getItDireccion(),
                itRotacion);
    }

    @Override
    public PosicionDireccion clone() {
        PosicionDireccion nc = new PosicionDireccion(getX(), getY(), getDireccion());
        return nc;
    }

    @Override
    public String toString() {
        return this.getX() + ":" + this.getY() + "-" + this.getDireccion();
    }

}
