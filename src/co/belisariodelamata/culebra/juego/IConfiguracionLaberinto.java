/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.belisariodelamata.culebra.juego;

/**
 *
 * @author BELSOFT
 */
public interface IConfiguracionLaberinto {

    void siguientePosicion(PosicionDireccion parteCuerpo);

    boolean isCasillaOcupada(int x, int y);

    int getPosicionesEnX();

    int getPosicionesEnY();
}
