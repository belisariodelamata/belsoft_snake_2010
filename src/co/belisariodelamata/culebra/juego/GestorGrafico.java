/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.belisariodelamata.culebra.juego;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;

/**
 *
 * @author BELSOFT
 */
public class GestorGrafico {

    //tamaño de los cuadros horizontal y vertical
    long tamanoCuadroX;
    long tamanoCuadroY;

    boolean dibujarDirecto = true;

    boolean borrarAntesDeDibujar = false;

    byte idFondo = 0;
    byte idBloque = 0;

    Image imagenBuffer;//temporal para el doble buffer
    Graphics graficoImagenBuffer; //Grafico de la Imagen Temporal

    Graphics graficoDePanelJuego; //Grafico para Dibujo Directo

    Image imagenParaDibujar;//imagen que se usara el proximo dibujo
    Image imagenParaDibujarAuxiliar;//utilizada para imagenes secundarias

    public void dibujarImagenAuxiliarEnGraficoPanel(int x, int y) {
        dibujarImagenAuxiliar(graficoDePanelJuego, x, y);
    }

    public void dibujarImagenAuxiliarEnGraficoBuffer(int x, int y) {
        dibujarImagenAuxiliar(graficoImagenBuffer, x, y);
    }

    public void dibujarImagenAuxiliar(Graphics graphics, int x, int y) {
        graphics.drawImage(imagenParaDibujarAuxiliar,
                (int) (tamanoCuadroX * x), (int) (tamanoCuadroY * y),
                (int) (tamanoCuadroX), (int) tamanoCuadroY, null);
    }

    public void dibujarImagenEnGraficoPanel(int x, int y) {
        dibujarImagen(graficoDePanelJuego, x, y);
    }

    public void dibujarImagenEnGraficoBuffer(int x, int y) {
        dibujarImagen(graficoImagenBuffer, x, y);
    }

    public void dibujarImagen(Graphics graphics, int x, int y) {
        graphics.drawImage(imagenParaDibujar,
                (int) (tamanoCuadroX * x), (int) (tamanoCuadroY * y),
                (int) (tamanoCuadroX), (int) tamanoCuadroY, null);
    }

    void inicializarMedidas(JPanel panelJuego, int cuadrosX, int cuadrosY) {
        tamanoCuadroX = (long) panelJuego.getWidth() / cuadrosX;
        tamanoCuadroY = (long) panelJuego.getHeight() / cuadrosY;

        graficoDePanelJuego = panelJuego.getGraphics();

        imagenBuffer = panelJuego.createImage(panelJuego.getWidth(), panelJuego.getHeight());
        graficoImagenBuffer = imagenBuffer.getGraphics();

        idFondo = (byte) (Math.random() * 4 + 1);
        idBloque = (byte) (Math.random() * 4 + 1);

    }

}
