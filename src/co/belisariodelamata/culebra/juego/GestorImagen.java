package co.belisariodelamata.culebra.juego;

import java.awt.Image;
import java.util.HashMap;

/**
 *
 * @author BELSOFT
 */
public class GestorImagen {

    HashMap<String, Image> mapaImagenes = new HashMap<>();

    private Image getImagen(String rutaRecurso) {
        Image imagen = mapaImagenes.get(rutaRecurso);
        if (imagen == null) {
            imagen = new javax.swing.ImageIcon(getClass().getResource(rutaRecurso)).getImage();
            if (imagen == null) {
                System.out.println(rutaRecurso);
            }
            mapaImagenes.put(rutaRecurso, imagen);
        }
        return imagen;
    }

    public Image getImagenCuerpo(String nombreImagen) {
        return getImagen("/co/belisariodelamata/culebra/imagen/cuerpo/" + nombreImagen + ".png");
    }

    private Image getImagenExtra(String nombreImagen) {
        return getImagen("/co/belisariodelamata/culebra/imagen/extra/" + nombreImagen + ".png");
    }

    public Image getImagenBloque(byte itBloque) {
        return getImagenExtra("bloque" + itBloque);
    }

    public Image getImagenFondo(byte itFondo) {
        return getImagenExtra("fondo" + itFondo);
    }

    public Image getImagenBono() {
        return getImagenExtra("Bono");
    }

    public Image getImagenVitamina() {
        return getImagenExtra("V1");
    }

    public Image getImagenOrigen() {
        return getImagenExtra("origen");
    }

    public Image getImagenDestino() {
        return getImagenExtra("destino");
    }

    public Image getImagenAmplitud() {
        return getImagenExtra("amplitud");
    }

    public Image getImagenBono(int itMovimiento) {
        return getImagenExtra(getNombreImagenBono(itMovimiento));
    }

    public String getNombreImagenBono(int itMovimiento) {
        return "r" + itMovimiento;
    }

    public Image getImagenVitamina(int itMovimiento) {
        return getImagenExtra("r" + itMovimiento);
    }

    public Image getImagenBordeBloque(int itDireccion) {
        return getImagenExtra("bordeBloque" + itDireccion);
    }

    public Image getImagenCola(int direccion, int itMovimiento) {
        return getImagenCuerpo("q" + direccion + "-" + itMovimiento);
    }

    public Image getImagenCabeza(int direccion, int itMovimiento) {
        return getImagenCuerpo("h" + direccion + "-" + itMovimiento);
    }

    public Image getImagenCuerpoCruceLleno(int direccion1, int direccion2) {
        return getImagenCuerpoCruce("cl", direccion1, direccion2);
    }

    public Image getImagenCuerpoCruceNormal(int direccion1, int direccion2) {
        return getImagenCuerpoCruce("c", direccion1, direccion2);
    }

    private Image getImagenCuerpoCruce(String prefijo, int direccion1, int direccion2) {
        /**
         * Para realizar los cruces existe el siguiente patrón de las
         * direcciones. Cada pareja corresponde la primera y segunda dirección
         * separadas por el símbolo guion. Y están en sentido de las manecillas
         * del reloj. <br>
         * 3-2,4-3,1-4,2-1.
         *
         * Así mismo las parejas anteriores tienen una equivalencia pero cuando
         * las direcciones están en sentido de las manecillas del Reloj.<br>
         * 2-3,1-2,3-4,4-1
         *
         * Dicho lo anterior hay que hacer la equivalencia de las mismas, es
         * decir: <br>
         * 1-2 convertirlo a 4-3<br>
         * 2-3 convertirlo a 1-4<br>
         * 3-4 convertirlo a 2-1<br>
         * 4-1 convertirlo a 3-2
         *
         */

        //Se suma un valor a la Direccion1 para verificar si las direcciones
        //están el sentido de las manecillas del Reloj
        int tmpDireccion = Direccion.getDireccionInversaRelativa(direccion1);
        //Se confirma que el Cruce Se dio en Sentido a las Manecillas del Reloj
        if (tmpDireccion == direccion2) {
            direccion1 = Direccion.getDireccionNormalRelativa(direccion1);
            direccion2 = Direccion.getDireccionInversaRelativa(direccion2);
        }
        tmpDireccion = direccion2;

        return getImagenCuerpo(prefijo + direccion1 + "-" + tmpDireccion);
    }

    public Image getImagenCuerpo(int itDireccion, boolean conLlenura, int itMovimiento) {
        if (itDireccion == Direccion.ABAJO.getItDireccion()
                || itDireccion == Direccion.ARRIBA.getItDireccion()) {
            itDireccion = Direccion.ABAJO.getItDireccion();
        } else if (itDireccion == Direccion.DERECHA.getItDireccion()
                || itDireccion == Direccion.IZQUIERDA.getItDireccion()) {
            itDireccion = Direccion.DERECHA.getItDireccion();
        }
        return getImagenCuerpo(
                (conLlenura ? "cl" : "cn")
                + itDireccion
                + (conLlenura ? "" : "-" + itMovimiento)
        );
    }

}
