/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.belisariodelamata.culebra.juego;

import co.belisariodelamata.culebra.busqueda.BusquedaDireccion;
import co.belisariodelamata.culebra.busqueda.Ruta;
import co.belisariodelamata.culebra.laberinto.FactoryLaberinto;
import co.belisariodelamata.culebra.laberinto.IGeneradorLaberinto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Juego implements IConfiguracionLaberinto {

    public enum Modo {
        CLASICO, CAMPANA
    }

    private enum Alimento {
        VITAMINA, BONO
    }

    private enum ModoInicioLaberinto {
        NUEVO_JUEGO, CONTINUAR_POR_CAMPANA
    }

    public enum Estado {
        NO_INICIADO, JUGANDO, PAUSADO, TERMINADO
    }

    private final int TAMANO_INICIAL_CULEBRA = 3;

    /**
     * Cantidad de Vitaminas que son necesarias en Campaña para pasar al
     * siguiente Laberinto
     */
    private final int VITAMINAS_MAXIMAS_DE_LABERINTO_CAMPANA = 10;
    /**
     * Laberinto máximo para rotar en campaña
     */
    public final int LABERINTO_MAXIMO_CAMPANA = 7;

    public final int VITAMINAS_PARA_BONO = 5;

    private static final int MINIMO_DE_FILAS = 12;
    private static final int MINIMO_DE_COLUMNAS = 22;

    private final Posicion posicionBono = new Posicion();
    private final Posicion posicionVitamina = new Posicion();
    ////////////////////////////////////
    private byte movimientoCabeza = 0;
    private byte movimientoCuerpo = 0;
    private byte movimientoCola = 0;

    /**
     * El Modo que está seleccionado para el juego
     */
    private Modo modoDeJuego = Modo.CLASICO;
    /**
     * El Nivel que está seleccionado para el Juego
     */
    private byte nivelActual = 11;//

    /**
     * El Nivel que está seleccionado para el Modo Clasico
     */
    private byte nivelClasico = 11;
    /**
     * El nivel que está seleccionado para el Modo Campaña
     */
    private byte nivelCampana = 11;
    private byte laberintoActualClasico = 0;
    private byte laberintoActualCampana = 0;

    private byte progresoCampana;
    private byte numeroVitaminas;
    private byte numeroVitaminasEnCampana;

    private Estado estado = Estado.NO_INICIADO;

    //valida cuando se ejecuta el timerEspera para no estrellarse
    private boolean esperandoPausaParcial = false;
    //puede pausar el juego
    private boolean puedePausar = false;
    private boolean puedeMoverLaCulebra = false;
    //guarda si en el siguiente movimiento se tendra que pausar
    private boolean proximoPausar = false;
    private int teclaPendientePorAplicar;

    private boolean busquedaAutomatica = false;

    private int puntaje;
    private byte tiempoBono;
    private boolean salioBono;
    private boolean comioVitamina = false;//comio VITAMINA
    private boolean mostrarLlenura = false;//mostrar la barriga
    private boolean mostrarDireccion = false;//mostrar direccion
    private boolean crecerAlComer = true;

    private Culebra culebra = new Culebra();

    public PosicionDireccion origen = new PosicionDireccion();
    public PosicionDireccion destino = new PosicionDireccion();

    private javax.swing.JLabel lblBono = null;
    private javax.swing.JLabel lblPuntaje = null;
    private javax.swing.JLabel lblProgresoVitamina = null;
    private javax.swing.JFrame formulario = null;
    ////////////////////////////
    ////////////////////////////
    private int cuadrosX, cuadrosY;//cantidad de los cuadros horizontal y vertical

    /**
     * La Casilla puede estar ocupada por un bloque, una vitamina, un bono o la
     * culebra
     */
    private boolean[][] casillaOcupada;
    private int casillasLibres;//guardará cuantos espacios están Vacios;

    private JPanel panelJuego = null;

    private final GestorImagen gestorImagenes = new GestorImagen();
    private final GestorGrafico gestorGrafico = new GestorGrafico();

    private boolean mostrarLaberintoEnConsola = true;

    public Juego() {
        timerEspera.setRepeats(false);
    }

    public void nuevoJuego() {
        this.iniciar(ModoInicioLaberinto.NUEVO_JUEGO);
    }

    private void iniciar(ModoInicioLaberinto forma) {
        this.culebra = new Culebra();
        inicializarMedidas(MINIMO_DE_COLUMNAS, MINIMO_DE_FILAS);
        this.getPosicionBono().inicializar();
        this.getPosicionVitamina().inicializar();

        Integer nuevoTiempoParaElCicloDelJuevo = null;
        if (forma.equals(ModoInicioLaberinto.NUEVO_JUEGO)) {
            this.numeroVitaminas = 0;
            this.numeroVitaminasEnCampana = 0;
            this.puntaje = 0;
            if (this.getModoDeJuego().equals(Modo.CLASICO)) {
                nuevoTiempoParaElCicloDelJuevo = (int) this.getNivelClasico();
            } else {
                nuevoTiempoParaElCicloDelJuevo = (int) this.getNivelCampana();
                this.progresoCampana = VITAMINAS_MAXIMAS_DE_LABERINTO_CAMPANA;
                this.laberintoActualCampana = -1;
            }
        }
        setFormularioTitulo("");

        int idlabDibujar;
        if (this.getModoDeJuego().equals(Modo.CAMPANA)) {//si se va a iniciar o continuar el modoDeJuego campaña
            this.numeroVitaminasEnCampana = 0;//no ha comido vitaminas en este laberinto
            this.laberintoActualCampana++;//cambia de laberinto hacia el siguiente
            if (this.getLaberintoActualCampana() > LABERINTO_MAXIMO_CAMPANA) {
                this.laberintoActualCampana = 0;//retorna al primero
                this.progresoCampana += VITAMINAS_MAXIMAS_DE_LABERINTO_CAMPANA;
                nuevoTiempoParaElCicloDelJuevo = this.getNivelActual() + (this.progresoCampana / 10);
            }
            idlabDibujar = this.getLaberintoActualCampana();
        } else {
            idlabDibujar = this.getLaberintoActualClasico();
        }
        IGeneradorLaberinto generadorLaberinto = obtenerLaberinto(idlabDibujar + "");
        dibujarLaberinto(generadorLaberinto);
        inicializarPosicionCulebra(generadorLaberinto);

        if (nuevoTiempoParaElCicloDelJuevo != null) {
            timerCicloJuego.setDelay(intervalSnake(nuevoTiempoParaElCicloDelJuevo));
        }
        this.estado = Estado.JUGANDO;
        this.mostrarDireccion = false;
        this.mostrarLlenura = false;
        this.comioVitamina = false;
        this.tiempoBono = 0;
        this.salioBono = false;
        //Primero se Dibuja la Culebra Inicial y después se genera la vitamina
        //Si se hace al reves la vitamina podría aparecer en la posición de la culebra
        dibujarCulebra();
        generarVitamina();
    }

    /**
     * Inicializa la posición de la Culebra teniendo en cuenta el Laberinto
     *
     * @param idLaberinto
     */
    void inicializarPosicionCulebra(IGeneradorLaberinto laberinto) {
        //Se ubica la Cabeza de la Culebra teniendo en cuenta el Id del Laberinto
        PosicionDireccion cabezaCulebra = laberinto.getPosicionCabezaCulebra();
        //Se le asigna la Cabeza a la Culebra
        getCulebra().setCabeza(cabezaCulebra);
        //Se simula que la Culebra comio Vitaminas para que logre el tamano inicial
        for (int i = 1; i < TAMANO_INICIAL_CULEBRA; i++) {
            this.comioVitamina = true;
            moverCrecerCulebra();
        }
    }

    private void generarVitamina() {
        generarVitaminaYBono(Alimento.VITAMINA);
    }

    private void generarBono() {
        generarVitaminaYBono(Alimento.BONO);
    }

    private void generarVitaminaYBono(Alimento alimento) {
        pausarTemporizadores();
        if (this.getEspaciosNoOcupados() == 1) {
            if (salioBono) {
                this.borrarCuadro(getPosicionBono().getX(), getPosicionBono().getY());
                getPosicionBono().inicializar();
                salioBono = false;
            }
        }
        int x, y;
        do {
            x = (int) (Math.random() * cuadrosX);
            y = (int) (Math.random() * cuadrosY);
        } while (getCuadrocupado(x, y) == true);//repite hasta que la VITAMINA aparezca en uno no ocupado

        if (alimento.equals(Alimento.VITAMINA)) {
            gestorGrafico.imagenParaDibujar = gestorImagenes.getImagenVitamina();
            getPosicionVitamina().setXY(x, y);
            dibujarCuadro(x, y);
        } else if (alimento.equals(Alimento.BONO)) {
            gestorGrafico.imagenParaDibujar = gestorImagenes.getImagenBono();
            getPosicionBono().setXY(x, y);
            dibujarCuadro(x, y);
        }
        reanudarTemporizadores();
    }

    void moverCrecerCulebra() {
        //Se añade una nuevaCabeza con las Características de la Anterior
        PosicionDireccion nuevaCabeza = getCulebra().agregarCabeza();
        //Se mueve la cabeza en la misma direccion
        siguientePosicion(nuevaCabeza);
        if (comioVitamina) {
            comioVitamina = false;
        } else {
            getCulebra().eliminarCola();
            borrarCuadroColaEliminada();
            dibujarCuadroCola();
        }
    }

    public void keyPressed(int keyCode) {
        //No se Permite el control de la culebra si no está jugando
        if (!(estado.equals(Estado.JUGANDO) || estado.equals(Estado.PAUSADO))) {
            return;
        }
        PosicionDireccion posicionDeCabeza = this.getCulebra().getCabeza();
        int itDireccion = posicionDeCabeza.getDireccion().getItDireccion();
        switch (keyCode) {
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_UP:
            case KeyEvent.VK_DOWN:
                if (puedeMoverLaCulebra || esperandoPausaParcial) {
                    detenerTimerEspera();
                    puedePausar = false;
                    puedeMoverLaCulebra = false;
                    posicionDeCabeza.cambiarDireccionAbsoluta(convertirDireccion(keyCode));
                    if (itDireccion != posicionDeCabeza.getDireccion().getItDireccion()) {
                        mostrarDireccion = true;
                    }
                    if (estado.equals(Estado.PAUSADO)) {
                        setFormularioTitulo("");
                        estado = Estado.JUGANDO;
                        reanudarTemporizadores();
                    } else if (esperandoPausaParcial) {
                        esperandoPausaParcial = false;
                        reanudarTemporizadores();
                    }
                } else {
                    teclaPendientePorAplicar = keyCode;
                }
                break;

            case KeyEvent.VK_ENTER:
                if (puedePausar || esperandoPausaParcial) {
                    pausar();
                } else if (!estado.equals(Estado.PAUSADO)) {
                    proximoPausar = true;
                }
        }
    }

    /**
     * La culebra no es lineal sino que tiene el cuerpo con curvas. Esto se
     * logra con unos iteradores que determinan la imagen a mostrar,
     * 1,2,1,2,1,2,1,2 XD. Este metodo cambiar los iteradores
     */
    private void iterarContadores() {
        movimientoCabeza++;
        movimientoCuerpo++;
        if (comioVitamina == false) {
            movimientoCola++;
            if (movimientoCola == 2) {
                movimientoCola = 0;
            }
        }
        if (movimientoCabeza >= 2) {
            movimientoCabeza = 0;
            movimientoCuerpo = 0;
        }
    }

    void verificarVisualizacionBono() {
        if (this.salioBono) {
            this.tiempoBono--;
            if (this.tiempoBono == 0) {
                this.numeroVitaminas = 0;
                borrarCuadro(this.getPosicionBono());
                this.getPosicionBono().inicializar();
                this.salioBono = false;
            }
        }
    }

    void refrescarEtiquetas() {
        lblBono.setVisible(this.salioBono);
        lblBono.setText("Bono: " + this.tiempoBono);
        lblPuntaje.setText("Puntaje: " + this.puntaje);
        lblProgresoVitamina.setVisible(modoDeJuego.equals(Modo.CAMPANA));
        lblProgresoVitamina.setText("Vitaminas: " + (this.progresoCampana - this.numeroVitaminasEnCampana));

    }

    void verificarPosicionActualCabeza() {
        //Se comprueba si la cabeza está en un lugar ocupado
        if (getCuadrocupado(getCulebra().getCabeza())) {
            if (getCulebra().getCabeza().equals(this.getPosicionVitamina())) {
                this.comioVitamina = true;
                this.mostrarLlenura = true;
                this.dibujarCuadroCabeza();

                this.puntaje += this.getNivelActual();
                this.numeroVitaminas++;
                if (this.getModoDeJuego().equals(Modo.CAMPANA)) {
                    this.numeroVitaminasEnCampana++;
                    if (this.numeroVitaminasEnCampana == this.progresoCampana) {
                        this.numeroVitaminas = 1;
                        pausarTemporizadores();
                        this.teclaPendientePorAplicar = 0;
                        iniciar(ModoInicioLaberinto.CONTINUAR_POR_CAMPANA);
                        return;
                    }
                }

                if (getEspaciosNoOcupados() - 1 == 0) {
                    generarColision();
                    return;
                } else {
                    generarVitamina();
                }
                if (this.numeroVitaminas >= VITAMINAS_PARA_BONO) {
                    generarBono();
                    this.salioBono = true;
                    this.tiempoBono = (byte) (getScaleWidth() - 1);
                    this.numeroVitaminas = 0;
                }

            } else if (getCulebra().getCabeza().equals(this.getPosicionBono())) {
                this.numeroVitaminas = 0;
                this.mostrarLlenura = true;
                this.dibujarCuadroCabeza();
                this.puntaje += 5 + (5 * this.getNivelActual()) + (2 * this.tiempoBono);
                this.salioBono = false;
                this.tiempoBono = 0;
                this.getPosicionBono().inicializar();
            } else {
                //Se encontró con un bloque o con la misma culebra
                generarColision();
            }
        }
    }

    void verificarPosicionSiguienteCabeza() {
        if (estado.equals(Estado.TERMINADO)) {
            return;
        }
        PosicionDireccion proximaCabeza = getCulebra().getCabeza().clone();//copia de la cabeza
        siguientePosicion(proximaCabeza);//siguiente posicion a la copia de la cabeza
        if (proximaCabeza.equals(this.getPosicionBono())) {//Si en el siguiente movimiento se encontrara con un posicionBono
            dibujarCuadroCabeza();
        } else if (proximaCabeza.equals(this.getPosicionVitamina())) {//Si en el siguiente movimiento se encontrara con un posicionBono
            dibujarCuadroCabeza();
        } else {
            dibujarCuadroCabeza();
            //Se está jugando, la proxima posición está ocupada y no es de la cola
            if (!(culebra.getCola().igualPosicion(proximaCabeza))
                    && getCuadrocupado(proximaCabeza)) {
                this.puedeMoverLaCulebra = true;
                this.puedePausar = true;
                this.esperandoPausaParcial = true;
                pausaParcial();
                if (isBusquedaAutomatica()) {
                    detectarDireccionParaElegir();
                }
            }
        }
    }

    void verificarEventosPendientes() {
        if (this.proximoPausar) {
            this.proximoPausar = false;
            keyPressed(KeyEvent.VK_ENTER);
        } else if (this.teclaPendientePorAplicar != 0) {
            keyPressed(this.teclaPendientePorAplicar);
            this.teclaPendientePorAplicar = 0;
        }
        this.puedeMoverLaCulebra = true;
        this.puedePausar = true;
    }

    /**
     * Movimiento lógico del Juego
     */
    void siguienteIteracion() {
        repintar();
        iterarContadores();
        verificarVisualizacionBono();
        moverCrecerCulebra();
        mostrarCrucesYDireccionesDelCuerpo();
        verificarPosicionActualCabeza();
        verificarPosicionSiguienteCabeza();
        verificarEventosPendientes();
        if (!isCrecerAlComer()) {
            comioVitamina = false;
        }
    }

    void cicloDelJuego() {
        siguienteIteracion();
        if (isBusquedaAutomatica()) {
            detectarDireccionParaElegir();
        }
        refrescarEtiquetas();
    }

    public void pausar() {
        if (estado.equals(Estado.JUGANDO)) {
            pausarTemporizadores();
            puedePausar = false;
            estado = Estado.PAUSADO;
            setFormularioTitulo("(Pausado)");
        }
    }

    private void setFormularioTitulo(String titulo) {
        formulario.setTitle("Belsoft Snake" + titulo);
    }

    public void retomarJuego() {
        if (this.estado.equals(Estado.TERMINADO)) {
            this.estado = Estado.PAUSADO;
            if (this.isBusquedaAutomatica()) {
                reanudarTemporizadores();
            }
        } else if (this.estado.equals(Estado.PAUSADO)) {
            if (this.isBusquedaAutomatica()) {
                reanudarTemporizadores();
            }
        }
    }

    private void detectarDireccionParaElegir() {
        //Se crea un Objeto para Buscar Rutas
        Ruta ruta = new Ruta(this);
        //Se marca que la posición de la vitamina y la del bono en realidad están libres
        ruta.addPosicionesLibres(getPosicionVitamina(), getPosicionBono());
        /////////////////////////////////////
        Direccion direccion = null;
        if (salioBono) {
            //Busco el Menor número de casillas hacia el Bono
            direccion = ruta.seleccionarDireccion(culebra.getCabeza(), getPosicionBono(), (int) tiempoBono);
        }
        //Si no hay dirección hacia el Bono
        if (direccion == null) {
            //Busco el menor número de casillas hacia la vitamina
            direccion = ruta.seleccionarDireccion(culebra.getCabeza(), getPosicionVitamina());
            if (direccion == null) {
                //si no es posible llegar a la VITAMINA
                //intentaré llegar a la cola;
                direccion = ruta.seleccionarDireccion(culebra.getCabeza(), culebra.getCola());
            }
        }
        ////////////////////////////
        List<BusquedaDireccion> espaciosEnDirecciones = ruta.calcularEspaciosEnDireccion(culebra.getCabeza());
        int numeroEspaciosMaximo = espaciosEnDirecciones.get(0).getNumeroPasos();
        //Verificamos cuantas direcciones tienen la misma cantidad de espacios
        boolean direccionTieneUnMayorNumeroDeEspacios = false;
        int numeroDireccionesConIgualMayorCantidadDeEspacios = 0;
        for (BusquedaDireccion busquedaEspacios : espaciosEnDirecciones) {
            if (busquedaEspacios.getNumeroPasos() == numeroEspaciosMaximo) {
                numeroDireccionesConIgualMayorCantidadDeEspacios++;
                if (direccion != null && direccion.equals(busquedaEspacios.getDireccionInicial())) {
                    direccionTieneUnMayorNumeroDeEspacios = true;
                }
            }
        }
        if (!direccionTieneUnMayorNumeroDeEspacios) {
            direccion = espaciosEnDirecciones.get((int) (Math.random() * numeroDireccionesConIgualMayorCantidadDeEspacios)).getDireccionInicial();
        }

        ///////////////////////////
        if (direccion != null) {
            keyPressed(convertirDireccion(direccion.getItDireccion()));
        }
    }

    private void pausarTemporizadores() {
        timerEspera.stop();
        timerCicloJuego.stop();
    }

    private void reanudarTemporizadores() {
        timerCicloJuego.start();
    }

    private void detenerTimerEspera() {
        timerEspera.stop();
    }

    private void pausaParcial() {
        timerCicloJuego.stop();
        timerEspera.start();
    }

    Timer timerEspera = new javax.swing.Timer(200, new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            esperandoPausaParcial = false;
            timerCicloJuego.start();
        }
    });

    Timer timerCicloJuego = new Timer(20, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                cicloDelJuego();
            } catch (Exception ex) {
                System.out.println("Error raro");
                ex.printStackTrace();
            }
        }
    });

    public void setLblBono(javax.swing.JLabel lblbono) {
        this.lblBono = lblbono;
    }

    public void setLblPuntaje(javax.swing.JLabel lblpuntaje) {
        this.lblPuntaje = lblpuntaje;
    }

    public void setLblProgresoVitamina(javax.swing.JLabel lblprogresovit) {
        this.lblProgresoVitamina = lblprogresovit;
    }

    public void setPanelJuego(javax.swing.JPanel panelJuego) {
        this.panelJuego = panelJuego;
    }

    public void setFrame(javax.swing.JFrame frame) {
        this.formulario = frame;
    }

    public void presentacion() {
        inicializarMedidas(MINIMO_DE_COLUMNAS, MINIMO_DE_FILAS);
        dibujarBS();
    }

    public byte getNivelActual() {
        return nivelActual;
    }

    public byte getMovCabeza() {
        return movimientoCabeza;
    }

    public byte getMovCuerpo() {
        return movimientoCuerpo;
    }

    public byte getMovCola() {
        return movimientoCola;
    }

    public Posicion getPosicionVitamina() {
        return posicionVitamina;
    }

    public Posicion getPosicionBono() {
        return posicionBono;
    }

    public Culebra getCulebra() {
        return culebra;
    }

    private void dibujarCuadroCola() {
        gestorGrafico.borrarAntesDeDibujar = true;
        gestorGrafico.imagenParaDibujar = gestorImagenes.getImagenCola(getCulebra().getCola().getDireccion().getItDireccion(), getMovCola());
        dibujarCuadro(getCulebra().getCola().getX(), getCulebra().getCola().getY());
    }

    private void dibujarCuadroCabeza() {
        gestorGrafico.imagenParaDibujar = gestorImagenes.getImagenCabeza(getCulebra().getCabeza().getDireccion().getItDireccion(), getMovCabeza());
        dibujarCuadro(getCulebra().getCabeza().getX(), getCulebra().getCabeza().getY());
    }

    public void dibujarCulebra() {

        gestorGrafico.dibujarDirecto = false;

        for (int i = 0; i < this.getCulebra().cuerpo.size(); i++) {
            PosicionDireccion nc = this.getCulebra().cuerpo.get(i);
            ///////////////////////
            iterarContadores();
            /////////////////////
            if (i == 0) {
                dibujarCuadroCabeza();
            } else if (i == this.getCulebra().cuerpo.size() - 1) {
                dibujarCuadroCola();
            } else {
                gestorGrafico.imagenParaDibujar
                        = gestorImagenes.getImagenCuerpo(nc.getDireccion().getItDireccion(), false, getMovCuerpo());
                dibujarCuadro(nc.getX(), nc.getY());
            }
        }
        repintar();
        gestorGrafico.dibujarDirecto = true;
    }

    public void inicializarMedidas(int cuadrosX, int cuadrosY) {
        this.cuadrosX = cuadrosX;
        this.cuadrosY = cuadrosY;

        casillaOcupada = new boolean[cuadrosX][cuadrosY];
        casillasLibres = cuadrosX * cuadrosY;

        gestorGrafico.inicializarMedidas(getPanelJuego(), cuadrosX, cuadrosY);

    }

    public void repintar() {
        if (getPanelJuego() != null) {
            getPanelJuego().getGraphics().drawImage(gestorGrafico.imagenBuffer, 0, 0, null);
            if (isMostrarEnConsola()) {
                System.out.println(this.getGraficoAscii());
            }
        }
    }

    public void establecerFondo() {
        for (int i = 0; i < this.getScaleWidth(); i++) {
            for (int j = 0; j < this.getScaleHeight(); j++) {
                borrarCuadro(i, j);
            }
        }
    }

    public int getScaleHeight() {
        return cuadrosY;
    }

    public int getScaleWidth() {
        return cuadrosX;
    }

    private void dibujarBloquesLaberinto(IGeneradorLaberinto iGeneradorLaberinto) {
        for (Posicion posicion : iGeneradorLaberinto.getListaBloques()) {
            dibujarBloqueCoordenadaJuego(posicion.getX(), posicion.getY());
        }
    }

    private IGeneradorLaberinto obtenerLaberinto(String sufijoLaberinto) {
        return FactoryLaberinto.getGeneradorLaberinto(sufijoLaberinto);
    }

    private IGeneradorLaberinto obtenerLaberinto(int numeroLaberinto) {
        return obtenerLaberinto(numeroLaberinto + "");
    }

    public void dibujarBS() {
        establecerFondo();
        gestorGrafico.dibujarDirecto = false;
        dibujarBloquesLaberinto(obtenerLaberinto("BS"));
        repintar();
        gestorGrafico.dibujarDirecto = true;
    }

    public void dibujarCuadro(int x, int y) {
        //necesario para que no quede la imagen debajo, efecto dado por la transparencia
        if (gestorGrafico.borrarAntesDeDibujar) {
            borrarCuadro(x, y, false);
        }
        if (gestorGrafico.dibujarDirecto && gestorGrafico.graficoDePanelJuego != null) {
            gestorGrafico.dibujarImagenEnGraficoPanel(x, y);
        }

        gestorGrafico.dibujarImagenEnGraficoBuffer(x, y);

        //Si no estaba dibujado el cuadro
        if (getCuadrocupado()[x][y] == false) {
            casillaOcupada[x][y] = true;
            casillasLibres--;
        }
    }

    public void dibujarCuadro(Posicion nc) {
        dibujarCuadro(nc.getX(), nc.getY());
    }

    /**
     *
     * @param x Pixeles en X del Laberinto
     * @param y Pixeles en Y del Laberinto
     * @return La posición del Laberinto teniendo en cuenta una coordenada de
     * Pixeles
     */
    public Posicion getCoordenadaDeLaberintoDeCoordenadaReal(int x, int y) {
        x = (int) (x / gestorGrafico.tamanoCuadroX);
        y = (int) (y / gestorGrafico.tamanoCuadroY);
        return new Posicion(x, y);
    }

    public void validarYBorrarBloqueCoordenadaPixel(int x, int y) {
        if (!isCulebraBonoVitamina(getCoordenadaDeLaberintoDeCoordenadaReal(x, y))) {
            borrarCuadroCoordenadaReal(x, y);
        }
    }

    private void borrarCuadroCoordenadaReal(int x, int y) {
        borrarCuadro(getCoordenadaDeLaberintoDeCoordenadaReal(x, y));
    }

    public void borrarCuadro(int x, int y) {
        borrarCuadro(x, y, true);
    }

    public void borrarCuadro(int x, int y, boolean desocuparCasilla) {
        gestorGrafico.imagenParaDibujarAuxiliar = gestorImagenes.getImagenFondo(gestorGrafico.idFondo);
        gestorGrafico.dibujarImagenAuxiliarEnGraficoPanel(x, y);
        gestorGrafico.dibujarImagenAuxiliarEnGraficoBuffer(x, y);
        //si el cuadro no estaba dibujado
        if (getCuadrocupado()[x][y] == true && desocuparCasilla) {
            //establecemos que hicimos un dibujo en el
            casillaOcupada[x][y] = false;
            //vamos relacionando internamente cuantos cuadros faltan para que el cuadro se llene
            casillasLibres++;
        }

    }

    public void borrarCuadro(Posicion posicion) {
        borrarCuadro(posicion.getX(), posicion.getY());
    }

    public void borrarCuadroColaEliminada() {
        borrarCuadro(getCulebra().ultimaColaEliminada().getX(), getCulebra().ultimaColaEliminada().getY());
    }

    public void mostrarCuerpo(Direccion direccion, boolean mostrarLlenura) {
        mostrarCuerpo((byte) direccion.getItDireccion(), mostrarLlenura);
    }

    public void mostrarCuerpo(byte direccion, boolean mostrarLlenura) {
        PosicionDireccion parteCuerpo = getCulebra().cuerpo.get(1);
        gestorGrafico.imagenParaDibujar
                = gestorImagenes.getImagenCuerpo(direccion, mostrarLlenura, movimientoCuerpo);
        dibujarCuadro(parteCuerpo);
    }

    private void mostrarCrucesYDireccionesDelCuerpo() {
        if (mostrarLlenura && mostrarDireccion == false) {
            gestorGrafico.borrarAntesDeDibujar = true;
            mostrarCuerpo(getCulebra().getCuerpoDespuesCabeza().getDireccion(), true);
            mostrarLlenura = false;
        } else if (mostrarLlenura && mostrarDireccion == true) {
            mostrarCruce(getCulebra().getCuerpoEnPosicion(1).getDireccion(), getCulebra().getCuerpoEnPosicion(2).getDireccion());
            mostrarLlenura = false;
            mostrarDireccion = false;
        } else if (mostrarDireccion) {
            mostrarCruce(getCulebra().getCuerpoEnPosicion(1).getDireccion(), getCulebra().getCuerpoEnPosicion(2).getDireccion());
            mostrarDireccion = false;
        } else {
            mostrarCuerpo(getCulebra().getCuerpoDespuesCabeza().getDireccion(), false);
        }
    }

    private void mostrarCruce(Direccion dir1, Direccion dir2) {
        PosicionDireccion nc = getCulebra().getCuerpoDespuesCabeza();
        gestorGrafico.borrarAntesDeDibujar = true;
        if (mostrarLlenura) {
            gestorGrafico.imagenParaDibujar = gestorImagenes.getImagenCuerpoCruceLleno(dir1.getItDireccion(), dir2.getItDireccion());
        } else {
            gestorGrafico.imagenParaDibujar = gestorImagenes.getImagenCuerpoCruceNormal(dir1.getItDireccion(), dir2.getItDireccion());
        }
        dibujarCuadro(nc.getX(), nc.getY());
        gestorGrafico.borrarAntesDeDibujar = false;
    }

    public void dibujarEstrellada() {
        gestorGrafico.borrarAntesDeDibujar = false;
        dibujarCuadroCabeza();
    }

    public boolean estaCuadroOcupadoPeroNoEsComida(Posicion nc) {
        if (getCuadrocupado()[nc.getX()][nc.getY()]) {
            return !(nc.equals(getPosicionBono()) || nc.equals(getPosicionVitamina()));
        } else {
            return false;
        }
    }

    public boolean getCuadrocupado(Posicion posicion) {
        return getCuadrocupado()[posicion.getX()][posicion.getY()];
    }

    public boolean getCuadrocupado(int x, int y) {
        return getCuadrocupado()[x][y];
    }

    public int getEspaciosNoOcupados() {
        return casillasLibres;
    }

    public void terminar() {
        if (estado.equals(Estado.NO_INICIADO)) {
            estado = Estado.TERMINADO;
        }
    }

    public void generarColision() {
        estado = Estado.TERMINADO;
        timerCicloJuego.stop();
        dibujarEstrellada();
    }

    public void borrarTodo() {
        casillaOcupada = new boolean[cuadrosX][cuadrosY];
        casillasLibres = cuadrosX * cuadrosY;
        gestorGrafico.graficoImagenBuffer.clearRect(0, 0, getPanelJuego().getWidth(), getPanelJuego().getHeight());
        gestorGrafico.graficoDePanelJuego.clearRect(0, 0, getPanelJuego().getWidth(), getPanelJuego().getHeight());
    }

    public void dibujarLaberintoMuestra(int laberinto) {
        this.culebra = new Culebra();
        this.getPosicionBono().inicializar();
        this.getPosicionVitamina().inicializar();
        dibujarLaberinto(obtenerLaberinto(laberinto));
    }

    private void dibujarLaberinto(int laberinto) {
        dibujarLaberinto(obtenerLaberinto(laberinto));
    }

    public void dibujarLaberinto(IGeneradorLaberinto laberinto) {
        borrarTodo();
        establecerFondo();
        gestorGrafico.imagenParaDibujar = gestorImagenes.getImagenBloque(gestorGrafico.idBloque);
        gestorGrafico.dibujarDirecto = false;
        dibujarBloquesLaberinto(laberinto);
        repintar();
        gestorGrafico.dibujarDirecto = true;
    }

    public int intervalSnake(int nivel) {
        int valor = 660;
        valor -= nivel * 45;
        if (valor <= 40) {
            valor = 40;
        }
        return valor;
    }

///////////////////////////////////
    public void validarYDibujarBloqueCoordenadaPixel(int x, int y) {
        x = (int) (x / gestorGrafico.tamanoCuadroX);
        y = (int) (y / gestorGrafico.tamanoCuadroY);
        if (!isCulebraBonoVitamina(new Posicion(x, y))) {
            dibujarBloqueCoordenadaJuego(x, y);
        }
    }

    public void dibujarBloqueCoordenadaPixel(int x, int y) {
        x = (int) (x / gestorGrafico.tamanoCuadroX);
        y = (int) (y / gestorGrafico.tamanoCuadroY);
        dibujarBloqueCoordenadaJuego(x, y);
    }

    private void dibujarBloqueCoordenadaJuego(int x, int y) {
        gestorGrafico.imagenParaDibujar = gestorImagenes.getImagenBloque(gestorGrafico.idBloque);
        dibujarCuadro(x, y);
        dibujarBordeBloque(x, y, true);
    }

    private void dibujarBloque(int x, int y, boolean redibujarBloquesConsecutivos) {
        gestorGrafico.imagenParaDibujar = gestorImagenes.getImagenBloque(gestorGrafico.idBloque);
        dibujarCuadro(x, y);
        dibujarBordeBloque(x, y, redibujarBloquesConsecutivos);
    }

    private void dibujarBordeBloque(int x, int y, boolean redibujarBloquesConsecutivos) {
        PosicionDireccion posicionDireccion = new PosicionDireccion();
        for (Direccion direccion : Direccion.values()) {
            posicionDireccion.setXY(x, y);
            posicionDireccion.setDireccion(direccion);
            siguientePosicion(posicionDireccion);
            if (!getCuadrocupado(posicionDireccion)
                    || isCulebraBonoVitamina(posicionDireccion)) {
                gestorGrafico.imagenParaDibujar = gestorImagenes.getImagenBordeBloque(direccion.getItDireccion());
                gestorGrafico.borrarAntesDeDibujar = false;
                dibujarCuadro(x, y);
            } else if (redibujarBloquesConsecutivos) {
                dibujarBloque(posicionDireccion.getX(), posicionDireccion.getY(), false);
            }
        }
    }

    //////////////////////
    /////////////////////
    public PosicionDireccion crearBloqueCoordenadaPixel(int x, int y) {
        return new PosicionDireccion((int) (x / gestorGrafico.tamanoCuadroX), (int) (y / gestorGrafico.tamanoCuadroY), 0);
    }

    public boolean[][] getCuadrocupado() {
        return casillaOcupada;
    }

    public boolean[][] getMatriz() {
        boolean matriz[][] = new boolean[this.casillaOcupada.length][this.casillaOcupada[0].length];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                matriz[i][j] = this.casillaOcupada[i][j];
            }
        }
        return matriz;
    }

    @Override
    public void siguientePosicion(PosicionDireccion parteCuerpo) {
        switch (parteCuerpo.getDireccion().getItDireccion()) {
            case 1:
                parteCuerpo.setY(parteCuerpo.getY() + 1);
                if (parteCuerpo.getY() >= getScaleHeight()) {
                    parteCuerpo.setY(0);
                }
                break;
            case 2:
                parteCuerpo.setX(parteCuerpo.getX() + 1);
                if (parteCuerpo.getX() >= getScaleWidth()) {
                    parteCuerpo.setX(0);
                }
                break;
            case 3:
                parteCuerpo.setY(parteCuerpo.getY() - 1);
                if (parteCuerpo.getY() < 0) {
                    parteCuerpo.setY(getScaleHeight() - 1);
                }
                break;
            case 4:
                parteCuerpo.setX(parteCuerpo.getX() - 1);
                if (parteCuerpo.getX() < 0) {
                    parteCuerpo.setX(getScaleWidth() - 1);
                }
                break;
        }
    }

    public boolean cuadrosJuntos(PosicionDireccion n1, PosicionDireccion n2) {
        for (int i = 1; i <= Direccion.values().length; i++) {
            PosicionDireccion nc = n1.clone();
            nc.setDireccion(i);
            siguientePosicion(nc);
            if (nc.igualPosicion(n2)) {
                return true;
            }
        }
        return false;
    }

    boolean isPosicionBono(int x, int y) {
        return this.getPosicionBono().equals(new Posicion(x, y));
    }

    boolean isPosicionVitamina(int x, int y) {
        return this.getPosicionVitamina().equals(new Posicion(x, y));
    }

    public String getGraficoAscii() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n-----------------------------------------");
        sb.append("\n");
        if (casillaOcupada != null) {
            for (int i = 0; i < casillaOcupada[0].length; i++) {
                sb.append("|");
                for (int j = 0; j < casillaOcupada.length; j++) {
                    if (culebra != null && culebra.tienePosicion(j, i)) {
                        sb.append("#");
                    } else if (isPosicionBono(j, i)) {
                        sb.append("B");
                    } else if (isPosicionVitamina(j, i)) {
                        sb.append("V");
                    } else if (casillaOcupada[j][i]) {
                        sb.append("*");
                    } else {
                        sb.append(" ");
                    }
                }
                sb.append("|\n");
            }
        }
        sb.append("-----------------------------------------");
        return sb.toString();
    }

    public Modo getModoDeJuego() {
        return modoDeJuego;
    }

    public void setModoDeJuego(Modo modoDeJuego) {
        this.modoDeJuego = modoDeJuego;
    }

    @Override
    public boolean isCasillaOcupada(int x, int y) {
        return this.casillaOcupada[x][y];
    }

    @Override
    public int getPosicionesEnX() {
        return this.cuadrosX;
    }

    @Override
    public int getPosicionesEnY() {
        return this.cuadrosY;
    }

    public boolean isCulebraBonoVitamina(Posicion posicion) {
        return (culebra != null && culebra.tienePosicion(posicion))
                || posicion.equals(getPosicionBono())
                || posicion.equals(getPosicionVitamina());
    }

    public int getLaberintoActualClasico() {
        return laberintoActualClasico;
    }

    public void setLaberintoActualClasico(byte laberintoActualClasico) {
        this.laberintoActualClasico = laberintoActualClasico;
    }

    public int getLaberintoActualCampana() {
        return laberintoActualCampana;
    }

    public int convertirDireccion(int keyCodeOItDireccion) {
        /**
         * El Código 41 es la Tecla de Referencia. 41-Tecla Izquierda=4,
         * 41-Tecla Arriba=3, 41-Tecla Derecha=2, 41-Tecla Abajo=1.
         *
         * Así mismo por Aritmetica si se resta el itDireccion daría la tecla.
         * 41-4=Tecla Izquierda, 41-3=Tecla Arriba, 41-2=Tecla Derecha,
         * 41-1=Tecla Abajo.
         */
        return 41 - keyCodeOItDireccion;
    }

    public byte getNivelClasico() {
        return nivelClasico;
    }

    public void setNivelClasico(byte nivelClasico) {
        this.nivelClasico = nivelClasico;
    }

    public byte getNivelCampana() {
        return nivelCampana;
    }

    public void setNivelCampana(byte nivelCampana) {
        this.nivelCampana = nivelCampana;
    }

    public void setNivelActual(byte nivelActual) {
        this.nivelActual = nivelActual;
    }

    public boolean isCrecerAlComer() {
        return crecerAlComer;
    }

    public void setCrecerAlComer(boolean crecerAlComer) {
        this.crecerAlComer = crecerAlComer;
    }

    public boolean isMostrarEnConsola() {
        return mostrarLaberintoEnConsola;
    }

    public void setMostrarEnConsola(boolean mostrarEnConsola) {
        this.mostrarLaberintoEnConsola = mostrarEnConsola;
    }

    public JPanel getPanelJuego() {
        return panelJuego;
    }

    public Estado getEstado() {
        return this.estado;
    }

    public boolean isBusquedaAutomatica() {
        return busquedaAutomatica;
    }

    public void setBusquedaAutomatica(boolean busquedaAutomatica) {
        this.busquedaAutomatica = busquedaAutomatica;
    }

}
