package co.belisariodelamata.culebra.juego;

/**
 * @author BELSOFT
 */
public class Posicion {

    protected static final int X_DEFAULT = -1;
    protected static final int Y_DEFAULT = -1;

    private int x;
    private int y;

    public Posicion() {
        this(X_DEFAULT, Y_DEFAULT);
    }

    public Posicion(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x, int y) {
        this.setX(x);
        this.setY(y);
    }

    public void inicializar() {
        setXY(Y_DEFAULT, Y_DEFAULT);
    }

    /**
     * Los objetos son equivalentes cuando tienen las mismas posiciones
     */
    @Override
    public boolean equals(Object obj) {
        Posicion posicion = (Posicion) obj;
        return this.x == posicion.x && this.y == posicion.y;
    }

    @Override
    public Posicion clone() {
        return new Posicion(getX(), getY());
    }

}
