package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.Posicion;
import java.util.List;

/**
 *
 * @author bdelam
 */
public class Laberinto6 extends AbstractGeneradorLaberinto {

    public Laberinto6() {
        posicionCabezaCulebra.setXY(12, 3);
    }

    @Override
    public List<Posicion> getListaBloques() {
        int x;
        agregarBloque(11, 0);
        agregarBloque(11, 1);
        agregarBloque(10, 2);
        agregarBloque(11, 2);
        agregarBloque(12, 2);
        agregarBloque(11, 3);
        agregarBloque(11, 4);
        agregarBloque(7, 5);
        agregarBloque(11, 5);
        agregarBloque(15, 5);
        for (x = 0; x <= 21; x++) {
            agregarBloque(x, 6);
        }
        agregarBloque(11, 7);
        agregarBloque(11, 8);
        agregarBloque(10, 9);
        agregarBloque(11, 9);
        agregarBloque(12, 9);
        agregarBloque(11, 10);
        agregarBloque(11, 11);
        return listaBloques;
    }
}
