package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.Posicion;
import java.util.List;

/**
 *
 * @author bdelam
 */
public class Laberinto3 extends AbstractGeneradorLaberinto {

    public Laberinto3() {
        posicionCabezaCulebra.setXY(1, 5);
    }

    @Override
    public List<Posicion> getListaBloques() {
        int x;
        agregarBloque(7, 0);
        agregarBloque(7, 1);
        agregarBloque(7, 2);
        for (x = 14; x <= 21; x++) {
            agregarBloque(x, 2);
        }
        agregarBloque(7, 3);
        agregarBloque(7, 4);
        agregarBloque(7, 5);
        agregarBloque(14, 6);
        agregarBloque(14, 7);
        agregarBloque(14, 8);
        for (x = 0; x <= 7; x++) {
            agregarBloque(x, 9);
        }
        agregarBloque(14, 9);
        agregarBloque(14, 10);
        agregarBloque(14, 11);
        return listaBloques;
    }
}
