package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.Posicion;
import java.util.List;

/**
 *
 * @author bdelam
 */
public class Laberinto8 extends AbstractGeneradorLaberinto {

    public Laberinto8() {
        posicionCabezaCulebra.setXY(0, 10);
    }

    @Override
    public List<Posicion> getListaBloques() {
        int x;
        agregarBloque(6, 0);
        agregarBloque(15, 0);
        agregarBloque(5, 1);
        agregarBloque(16, 1);
        agregarBloque(4, 2);
        agregarBloque(17, 2);
        agregarBloque(3, 3);
        agregarBloque(18, 3);
        agregarBloque(2, 4);
        agregarBloque(19, 4);
        agregarBloque(1, 5);
        agregarBloque(20, 5);
        agregarBloque(0, 6);
        agregarBloque(21, 6);
        for (x = 0; x <= 6; x++) {
            agregarBloque(x, 7);
        }
        for (x = 15; x <= 21; x++) {
            agregarBloque(x, 7);
        }
        agregarBloque(6, 8);
        agregarBloque(15, 8);
        return listaBloques;
    }
}
