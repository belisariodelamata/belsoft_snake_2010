package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.Posicion;
import java.util.List;

/**
 *
 * @author bdelam
 */
public class Laberinto9 extends AbstractGeneradorLaberinto {

    public Laberinto9() {
        posicionCabezaCulebra.setXY(0, 5);
    }

    @Override
    public List<Posicion> getListaBloques() {
        int x;
        agregarBloque(3, 1);
        agregarBloque(5, 1);
        agregarBloque(6, 1);
        agregarBloque(14, 1);
        agregarBloque(15, 1);
        agregarBloque(17, 1);
        agregarBloque(3, 2);
        agregarBloque(5, 2);
        agregarBloque(6, 2);
        agregarBloque(14, 2);
        agregarBloque(15, 2);
        agregarBloque(3, 3);
        for (x = 14; x <= 17; x++) {
            agregarBloque(x, 3);
        }
        for (x = 3; x <= 6; x++) {
            agregarBloque(x, 4);
        }
        for (x = 14; x <= 17; x++) {
            agregarBloque(x, 4);
        }
        for (x = 0; x <= 4; x++) {
            agregarBloque(x, 6);
        }
        agregarBloque(21, 6);
        agregarBloque(7, 7);
        agregarBloque(9, 7);
        agregarBloque(11, 7);
        for (x = 13; x <= 21; x++) {
            agregarBloque(x, 7);
        }
        agregarBloque(13, 8);
        agregarBloque(21, 9);
        for (x = 1; x <= 19; x++) {
            agregarBloque(x, 10);
        }
        agregarBloque(21, 10);
        agregarBloque(21, 11);
        return listaBloques;
    }
}
