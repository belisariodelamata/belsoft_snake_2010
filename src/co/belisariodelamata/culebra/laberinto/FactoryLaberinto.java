/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.belisariodelamata.culebra.laberinto;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bdelam
 */
public class FactoryLaberinto {

    private FactoryLaberinto() {

    }

    public static IGeneradorLaberinto getGeneradorLaberinto(String sufijo) {
        IGeneradorLaberinto iGeneradorLaberinto = null;
        try {
            iGeneradorLaberinto = (IGeneradorLaberinto) Class.forName(
                    FactoryLaberinto.class.getPackage().getName() + ".Laberinto" + sufijo).newInstance();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException ex) {
            Logger.getLogger(FactoryLaberinto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return iGeneradorLaberinto;
    }

}
