package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.Direccion;
import co.belisariodelamata.culebra.juego.PosicionDireccion;
import co.belisariodelamata.culebra.juego.Posicion;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author bdelam
 */
public abstract class AbstractGeneradorLaberinto implements IGeneradorLaberinto {

    PosicionDireccion posicionCabezaCulebra = new PosicionDireccion(Direccion.DERECHA);
    List<Posicion> listaBloques = new LinkedList<>();

    @Override
    public PosicionDireccion getPosicionCabezaCulebra() {
        return posicionCabezaCulebra;
    }

    protected void agregarBloque(int x, int y) {
        listaBloques.add(new Posicion(x, y));
    }

}
