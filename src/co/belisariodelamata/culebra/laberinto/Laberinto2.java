package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.Posicion;
import java.util.List;

/**
 *
 * @author bdelam
 */
public class Laberinto2 extends AbstractGeneradorLaberinto {

    public Laberinto2() {
        posicionCabezaCulebra.setXY(1, 5);
    }

    @Override
    public List<Posicion> getListaBloques() {
        int x;
        agregarBloque(0, 0);
        agregarBloque(1, 0);
        agregarBloque(20, 0);
        agregarBloque(21, 0);
        agregarBloque(0, 1);
        agregarBloque(21, 1);
        for (x = 6; x <= 15; x++) {
            agregarBloque(x, 4);
        }
        for (x = 6; x <= 15; x++) {
            agregarBloque(x, 7);
        }
        agregarBloque(0, 10);
        agregarBloque(21, 10);
        agregarBloque(0, 11);
        agregarBloque(1, 11);
        agregarBloque(20, 11);
        agregarBloque(21, 11);
        return listaBloques;
    }
}
