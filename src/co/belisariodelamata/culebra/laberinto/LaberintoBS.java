package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.Posicion;
import java.util.List;

/**
 *
 * @author bdelam
 */
public class LaberintoBS extends AbstractGeneradorLaberinto {

    public LaberintoBS() {
        posicionCabezaCulebra.setXY(0, 5);
    }
    
    @Override
    public List<Posicion> getListaBloques() {
        int x;
        agregarBloque(0, 0);
        agregarBloque(1, 0);
        agregarBloque(2, 0);
        for (x = 5; x <= 8; x++) {
            agregarBloque(x, 0);
        }
        agregarBloque(10, 0);
        for (x = 17; x <= 21; x++) {
            agregarBloque(21, 0);
        }
        agregarBloque(0, 1);
        agregarBloque(3, 1);
        agregarBloque(5, 1);
        agregarBloque(10, 1);
        agregarBloque(21, 1);
        agregarBloque(0, 2);
        agregarBloque(1, 2);
        agregarBloque(2, 2);
        agregarBloque(5, 2);
        agregarBloque(6, 2);
        agregarBloque(7, 2);
        agregarBloque(10, 2);
        agregarBloque(17, 2);
        agregarBloque(18, 2);
        agregarBloque(19, 2);
        agregarBloque(21, 2);
        agregarBloque(0, 3);
        agregarBloque(3, 3);
        agregarBloque(5, 3);
        agregarBloque(10, 3);
        agregarBloque(21, 3);
        agregarBloque(0, 4);
        agregarBloque(1, 4);
        agregarBloque(2, 4);
        for (x = 5; x <= 8; x++) {
            agregarBloque(x, 4);
        }
        for (x = 10; x <= 13; x++) {
            agregarBloque(x, 4);
        }
        agregarBloque(21, 4);
        agregarBloque(21, 5);
        agregarBloque(1, 6);
        agregarBloque(2, 6);
        agregarBloque(3, 6);
        agregarBloque(6, 6);
        agregarBloque(7, 6);
        for (x = 10; x <= 13; x++) {
            agregarBloque(x, 6);
        }
        agregarBloque(15, 6);
        agregarBloque(16, 6);
        agregarBloque(17, 6);
        agregarBloque(21, 6);
        agregarBloque(0, 7);
        agregarBloque(5, 7);
        agregarBloque(8, 7);
        agregarBloque(10, 7);
        agregarBloque(16, 7);
        agregarBloque(21, 7);
        agregarBloque(1, 8);
        agregarBloque(2, 8);
        agregarBloque(5, 8);
        agregarBloque(8, 8);
        agregarBloque(10, 8);
        agregarBloque(11, 8);
        agregarBloque(12, 8);
        agregarBloque(16, 8);
        agregarBloque(21, 8);
        agregarBloque(3, 9);
        agregarBloque(5, 9);
        agregarBloque(8, 9);
        agregarBloque(10, 9);
        agregarBloque(16, 9);
        agregarBloque(21, 9);
        agregarBloque(0, 10);
        agregarBloque(1, 10);
        agregarBloque(2, 10);
        agregarBloque(6, 10);
        agregarBloque(7, 10);
        agregarBloque(10, 10);
        agregarBloque(16, 10);
        agregarBloque(21, 10);
        for (x = 18; x <= 21; x++) {
            agregarBloque(21, 11);
        }
        return listaBloques;
    }
}
