package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.Posicion;
import java.util.List;

/**
 *
 * @author bdelam
 */
public class Laberinto10 extends AbstractGeneradorLaberinto {

    public Laberinto10() {
        posicionCabezaCulebra.setXY(0, 3);
    }

    @Override
    public List<Posicion> getListaBloques() {
        int x;
        for (x = 0; x <= 5; x++) {
            agregarBloque(x, 2);
        }
        for (x = 13; x <= 21; x++) {
            agregarBloque(x, 2);
        }
        agregarBloque(9, 4);
        agregarBloque(10, 4);
        for (x = 8; x <= 11; x++) {
            agregarBloque(x, 5);
        }
        for (x = 7; x <= 12; x++) {
            agregarBloque(x, 6);
        }
        agregarBloque(0, 7);
        agregarBloque(1, 7);
        agregarBloque(3, 7);
        agregarBloque(4, 7);
        agregarBloque(7, 7);
        agregarBloque(12, 7);
        agregarBloque(14, 7);
        agregarBloque(15, 7);
        agregarBloque(17, 7);
        agregarBloque(18, 7);
        agregarBloque(20, 7);
        agregarBloque(21, 7);
        agregarBloque(7, 8);
        agregarBloque(12, 8);
        agregarBloque(7, 9);
        agregarBloque(12, 9);
        for (x = 1; x <= 7; x++) {
            agregarBloque(x, 10);
        }
        for (x = 12; x <= 20; x++) {
            agregarBloque(x, 10);
        }
        return listaBloques;
    }
}
