package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.Posicion;
import java.util.List;

/**
 *
 * @author bdelam
 */
public class Laberinto0 extends AbstractGeneradorLaberinto {

    public Laberinto0() {
        posicionCabezaCulebra.setXY(1, 5);
    }
    
    @Override
    public List<Posicion> getListaBloques() {

        return listaBloques;
    }
}
