package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.PosicionDireccion;
import co.belisariodelamata.culebra.juego.Posicion;
import java.util.List;

/**
 *
 * @author bdelam
 */
public interface IGeneradorLaberinto {

    List<Posicion> getListaBloques();

    PosicionDireccion getPosicionCabezaCulebra();
}
