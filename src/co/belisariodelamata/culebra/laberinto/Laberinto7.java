package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.Posicion;
import java.util.List;

/**
 *
 * @author bdelam
 */
public class Laberinto7 extends AbstractGeneradorLaberinto {

    public Laberinto7() {
        posicionCabezaCulebra.setXY(15, 0);
    }

    @Override
    public List<Posicion> getListaBloques() {
        int x;
        agregarBloque(12, 0);
        agregarBloque(13, 0);
        agregarBloque(14, 0);
        agregarBloque(12, 1);
        agregarBloque(3, 2);
        agregarBloque(7, 2);
        agregarBloque(12, 2);
        for (x = 0; x <= 9; x++) {
            agregarBloque(x, 3);
        }
        for (x = 12; x <= 18; x++) {
            agregarBloque(x, 3);
        }
        agregarBloque(12, 4);
        agregarBloque(7, 5);
        agregarBloque(7, 6);
        for (x = 2; x <= 7; x++) {
            agregarBloque(x, 7);
        }
        for (x = 10; x <= 21; x++) {
            agregarBloque(x, 7);
        }
        agregarBloque(7, 8);
        agregarBloque(12, 8);
        agregarBloque(17, 8);
        agregarBloque(7, 9);
        agregarBloque(12, 9);
        agregarBloque(5, 10);
        agregarBloque(6, 10);
        agregarBloque(7, 10);
        agregarBloque(7, 11);
        return listaBloques;
    }
}
