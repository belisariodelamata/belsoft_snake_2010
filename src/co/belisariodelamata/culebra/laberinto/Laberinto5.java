package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.Posicion;
import java.util.List;

/**
 *
 * @author bdelam
 */
public class Laberinto5 extends AbstractGeneradorLaberinto {

    public Laberinto5() {
        posicionCabezaCulebra.setXY(1, 5);
    }

    @Override
    public List<Posicion> getListaBloques() {
        int x;
        agregarBloque(0, 0);
        agregarBloque(1, 0);
        agregarBloque(2, 0);
        for (x = 5; x <= 18; x++) {
            agregarBloque(x, 0);
        }
        agregarBloque(0, 1);
        agregarBloque(9, 1);
        agregarBloque(9, 2);
        agregarBloque(9, 3);
        for (x = 0; x <= 9; x++) {
            if (x!=4){
                agregarBloque(x, 4);
            }
        }
        for (x = 12; x <= 21; x++) {
            agregarBloque(x, 4);
        }
        for (x = 0; x <= 21; x++) {
            agregarBloque(x, 7);
        }
        agregarBloque(11, 8);
        agregarBloque(11, 9);
        agregarBloque(11, 10);
        agregarBloque(11, 11);
        return listaBloques;
    }
}
