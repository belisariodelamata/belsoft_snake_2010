package co.belisariodelamata.culebra.laberinto;

import co.belisariodelamata.culebra.juego.Posicion;
import java.util.List;

/**
 *
 * @author bdelam
 */
public class Laberinto11 extends AbstractGeneradorLaberinto {

    public Laberinto11() {
        posicionCabezaCulebra.setXY(1, 5);
    }

    @Override
    public List<Posicion> getListaBloques() {
        int x;
        for (x = 0; x <= 21; x++) {
            agregarBloque(x, 0);
        }
        agregarBloque(0, 1);
        agregarBloque(21, 1);
        agregarBloque(0, 2);
        agregarBloque(4, 2);
        agregarBloque(7, 2);
        agregarBloque(10, 2);
        agregarBloque(13, 2);
        agregarBloque(16, 2);
        agregarBloque(19, 2);
        agregarBloque(21, 2);
        agregarBloque(0, 3);
        agregarBloque(21, 3);
        agregarBloque(0, 4);
        agregarBloque(21, 4);
        agregarBloque(0, 5);
        agregarBloque(21, 5);
        agregarBloque(0, 6);
        agregarBloque(21, 6);
        agregarBloque(0, 7);
        agregarBloque(21, 7);
        agregarBloque(0, 8);
        agregarBloque(21, 8);
        agregarBloque(0, 9);
        agregarBloque(2, 9);
        agregarBloque(5, 9);
        agregarBloque(8, 9);
        agregarBloque(11, 9);
        agregarBloque(14, 9);
        agregarBloque(17, 9);
        agregarBloque(21, 9);
        agregarBloque(0, 10);
        agregarBloque(21, 10);
        for (x = 0; x <= 21; x++) {
            agregarBloque(x, 11);
        }
        return listaBloques;
    }
}
